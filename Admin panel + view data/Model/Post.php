<?php
namespace OpenTag\HelloWorld\Model;
class Post extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'opentag_helloworld_post';

    protected $_cacheTag = 'opentag_helloworld_post';

    protected $_eventPrefix = 'opentag_helloworld_post';

    protected function _construct()
    {
        $this->_init('OpenTag\HelloWorld\Model\ResourceModel\Post');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}